package com.guoying.framework.security.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import com.guoying.common.core.domain.model.LoginUser;
import com.guoying.common.utils.SecurityUtils;
import com.guoying.common.utils.StringUtils;
import com.guoying.framework.web.service.TokenService;

/**
 * token过滤器 验证token有效性
 *
 * @author guoying
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter
{
    @Autowired
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {

        /**
         * 通过request的url判断是否为websocket请求
         * 如果为websocket请求，先处理Authorization
         */
        System.out.println("用户请求地址："+ request.getRequestURI());
        if(request.getRequestURI().contains("/websocket")){
            HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper((HttpServletRequest) request);
            //修改header信息 将websocket请求中的Sec-WebSocket-Protocol值取出处理后放入认证参数key
            //此处需要根据自己的认证方式进行修改  Authorization 和 Bearer
            System.out.println("token信息："+"Bearer "+request.getHeader("Sec-WebSocket-Protocol"));
            requestWrapper.addHeader("Authorization","Bearer "+request.getHeader("Sec-WebSocket-Protocol"));
            request = (HttpServletRequest)  requestWrapper;
        }
        LoginUser loginUser = tokenService.getLoginUser(request);
        System.out.println("获取用户信息："+ JSON.toJSONString(loginUser));
        if (StringUtils.isNotNull(loginUser) && StringUtils.isNull(SecurityUtils.getAuthentication()))
        {
            tokenService.verifyToken(loginUser);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        chain.doFilter(request, response);
    }
}
