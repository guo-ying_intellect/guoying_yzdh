package com.guoying;

import com.guoying.yzdh.aly.AmqpClientStart;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 启动程序
 *
 * @author guoying
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class GuoYingApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
//        SpringApplication.run(GuoYingApplication.class, args);
        ConfigurableApplicationContext run = SpringApplication.run(GuoYingApplication.class, args);
        AmqpClientStart.initSystemConfig(run);
        System.out.println("(♥◠‿◠)ﾉﾞ  国英启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
