package com.guoying.common.enums;

/**
 * 数据源
 *
 * @author guoying
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
