package com.guoying.yzdh.job;

import com.guoying.yzdh.aly.AmqpClient;
import com.guoying.yzdh.aly.ApplicationContextFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @description：amqp连接多线程
 * @author：Sunup
 * @date：2023/10/19 8:39
 */
@Slf4j
@Component
public class AmqpConnectThread implements Runnable {
    @Override
    public void run() {
            try {
                executeTask();
            } catch (Throwable e) {
                log.error("连接失败", e);
            }
    }

    private void executeTask() throws Exception{
        AmqpClient amqpClient = ApplicationContextFactory.getBean("amqpClient", AmqpClient.class);
        amqpClient.amqpStart();
    }

}
