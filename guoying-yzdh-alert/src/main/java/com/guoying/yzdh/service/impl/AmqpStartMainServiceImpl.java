package com.guoying.yzdh.service.impl;

import com.guoying.yzdh.job.AmqpConnectThread;
import com.guoying.yzdh.service.IAmqpStartMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @description：AmqpStart
 * @author：Sunup
 * @date：2023/10/19 8:49
 */
@Service
@Slf4j
public class AmqpStartMainServiceImpl implements IAmqpStartMainService {
    @Override
    public void amqpStart() {
        AmqpConnectThread amqpConnectThread = new AmqpConnectThread();
        Thread thread = new Thread(amqpConnectThread, "AmqpConnectThread");
        thread.start();
    }
}
