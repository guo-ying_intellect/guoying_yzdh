package com.guoying.yzdh.service.impl;

import com.alibaba.fastjson2.JSON;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.annotations.VisibleForTesting;
import com.guoying.common.core.domain.AjaxResult;
import com.guoying.common.utils.DateUtils;
import com.guoying.common.utils.StringUtils;
import com.guoying.yzdh.domain.BdzMonitorData;
import com.guoying.yzdh.domain.BdzYzdhAnylysisParam;
import com.guoying.yzdh.mapper.BdzMonitorDataMapper;
import com.guoying.yzdh.service.IBdzMonitorDataService;
import com.guoying.yzdh.service.IBdzYzdhAnylysisParamService;
import com.guoying.yzdh.service.WebSocketServer;
import com.guoying.yzdh.vomodel.YzdhAnalysisReqVo;
import com.guoying.yzdh.vomodel.YzdhAnalysisRespVo;
import com.guoying.yzdh.vomodel.YzdhMonitorDataQueryReqVo;
import com.guoying.yzdh.vomodel.YzdhMonitorDataVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 监测数据Service业务层处理
 *
 * @author guoying
 * @date 2023-10-16
 */
@Service
@Slf4j
public class BdzMonitorDataServiceImpl implements IBdzMonitorDataService {
    @Autowired
    private BdzMonitorDataMapper bdzMonitorDataMapper;

    @Autowired
    private IBdzYzdhAnylysisParamService bdzYzdhAnylysisParamService;

    @Autowired
    private WebSocketServer webSocketServer;

    /**
     * 查询监测数据
     *
     * @param dataId 监测数据主键
     * @return 监测数据
     */
    @Override
    public BdzMonitorData selectBdzMonitorDataByDataId(Long dataId) {
        return bdzMonitorDataMapper.selectBdzMonitorDataByDataId(dataId);
    }

    /**
     * 查询监测数据列表
     *
     * @param bdzMonitorData 监测数据
     * @return 监测数据
     */
    @Override
    public List<BdzMonitorData> selectBdzMonitorDataList(BdzMonitorData bdzMonitorData) {
        return bdzMonitorDataMapper.selectBdzMonitorDataList(bdzMonitorData);
    }

    /**
     * 新增监测数据
     *
     * @param bdzMonitorData 监测数据
     * @return 结果
     */
    @Override
    public int insertBdzMonitorData(BdzMonitorData bdzMonitorData) {
        bdzMonitorData.setCreateTime(DateUtils.getNowDate());
        return bdzMonitorDataMapper.insertBdzMonitorData(bdzMonitorData);
    }

    /**
     * 修改监测数据
     *
     * @param bdzMonitorData 监测数据
     * @return 结果
     */
    @Override
    public int updateBdzMonitorData(BdzMonitorData bdzMonitorData) {
        bdzMonitorData.setUpdateTime(DateUtils.getNowDate());
        return bdzMonitorDataMapper.updateBdzMonitorData(bdzMonitorData);
    }

    /**
     * 批量删除监测数据
     *
     * @param dataIds 需要删除的监测数据主键
     * @return 结果
     */
    @Override
    public int deleteBdzMonitorDataByDataIds(Long[] dataIds) {
        return bdzMonitorDataMapper.deleteBdzMonitorDataByDataIds(dataIds);
    }

    /**
     * 删除监测数据信息
     *
     * @param dataId 监测数据主键
     * @return 结果
     */
    @Override
    public int deleteBdzMonitorDataByDataId(Long dataId) {
        return bdzMonitorDataMapper.deleteBdzMonitorDataByDataId(dataId);
    }

    /**
     * @description：查询一址多户监测数据，默认查询24小时内的数据
     * @author：Sunup
     * @date：2023/10/19 8:27
     * @param：reqVo 查询条件
     */
    @Override
    public AjaxResult selectYzdhMonitorDataList(YzdhMonitorDataQueryReqVo reqVo) {
        log.info("查询一址多户监测数据入参：{}", JSON.toJSONString(reqVo));
        List<YzdhMonitorDataVo> dataVos = new ArrayList<>();
        try {
            if (StringUtils.isNull(reqVo.getQueryTime())) {
                reqVo.setQueryTime(DateUtils.getDate());
            }
            List<BdzMonitorData> monitorDataList = bdzMonitorDataMapper.selectYzdhMonitorDataList(reqVo);
            dataVos = getTransferMonitorDataList(monitorDataList);
//            log.info("查询一址多户监测数据返回结果：{}", JSON.toJSONString(dataVos));
            return AjaxResult.success(dataVos);
        } catch (Exception e) {
            log.error("查询一址多户监测数据出现异常，异常原因【{}】,异常详情：", e.getMessage(), e);
            return AjaxResult.error("查询一址多户监测数据失败");
        }
    }

    /**
     * @description：处理物联网返回的一址多户监测数据
     * @author：Sunup
     * @date：2023/10/19 8:26
     * @param：monitordata 物联网接口数据
     */
    @Override
    public void handleYzdhMonitorData(String monitordata) {
        log.info("处理物联网返回的一址多户监测数据：{}", monitordata);
        try {
            if (StringUtils.isNotEmpty(monitordata)) {
                JSONObject json=JSONObject.parseObject(monitordata);
                Long dateD=json.getLong("createTime")*1000;
                json.put("createTime",dateD);
                BdzMonitorData bdzMonitorData=json.toJavaObject(BdzMonitorData.class);
                if (StringUtils.isNotEmpty(bdzMonitorData.getDeviceNo())) {
//                    webSocketServer.sendMessage(monitordata);
                    if (StringUtils.isNull(bdzMonitorData.getCreateTime())) {
                        bdzMonitorData.setCreateTime(DateUtils.getNowDate());
                    }
                    bdzMonitorData.setCreateBy("System");
                    bdzMonitorDataMapper.insertBdzMonitorData(bdzMonitorData);
                }
            }
        } catch (Exception e) {
            log.error("处理物联网返回的一址多户监测数据出现异常，异常原因【{}】,异常详情：", e.getMessage(), e);
        }
    }

    /**
     * @description：分析一址多户监测结果
     * @author：Sunup
     * @date：2023/10/23 17:03
     * @param：
     */
    @Override
    public AjaxResult analysisYzdhResult(YzdhAnalysisReqVo yzdhAnalysisReqVo) {
        log.info("分析一址多户监测结果入参：{}", JSON.toJSONString(yzdhAnalysisReqVo));
        List<YzdhAnalysisRespVo> resultVos = new ArrayList<>();
        String analysisMsg = "";
        try {
            if (StringUtils.isNull(yzdhAnalysisReqVo)) {
                yzdhAnalysisReqVo = new YzdhAnalysisReqVo();
            }
            List<BdzYzdhAnylysisParam> params = bdzYzdhAnylysisParamService.selectBdzYzdhAnylysisParamList(new BdzYzdhAnylysisParam());
            if (StringUtils.isNotEmpty(params)) {
                String slope = "0.1", timeRange = "10";
                BdzYzdhAnylysisParam param = params.get(0);
                slope = param.getSlope();
                timeRange = param.getTimeRange();
                yzdhAnalysisReqVo.setSlope(slope);
                yzdhAnalysisReqVo.setTimeRange(timeRange);
            }
            if (StringUtils.isEmpty(yzdhAnalysisReqVo.getDateStart()) || StringUtils.isEmpty(yzdhAnalysisReqVo.getDateEnd())) {
                yzdhAnalysisReqVo.setDateEnd(DateUtils.dateTimeMinuteNow());
                Date now_10 = new Date((new Date()).getTime() - Long.valueOf(yzdhAnalysisReqVo.getTimeRange()) * 60000); //10分钟前的时间
                yzdhAnalysisReqVo.setDateStart(DateUtils.dateTimeMinute(now_10));
            }
            List<BdzMonitorData> monitorDataList = bdzMonitorDataMapper.analysisYzdhResult(yzdhAnalysisReqVo);
            List<YzdhMonitorDataVo> transferDatas = getTransferMonitorDataList(monitorDataList);
            if (StringUtils.isNotEmpty(transferDatas) && transferDatas.size() >= 2) {
                boolean isMeetRequire = false;
                for (int i = 0; i < transferDatas.size() - 1; i++) {
                    YzdhMonitorDataVo yzdhMonitorDataVo1 = transferDatas.get(i);
                    List list1 = yzdhMonitorDataVo1.getListMonitorData();
                    YzdhMonitorDataVo yzdhMonitorDataVo2 = transferDatas.get(i + 1);
                    List list2 = yzdhMonitorDataVo2.getListMonitorData();
                    if (list1.size() == list2.size()) {
                        isMeetRequire = true;
                        List<YzdhAnalysisRespVo> vos = analysisYzdhResult(yzdhMonitorDataVo1, yzdhMonitorDataVo2,
                                yzdhAnalysisReqVo.getSlope(), yzdhAnalysisReqVo.getTimeRange());
                        resultVos.addAll(vos);
                    }
                }
                if (!isMeetRequire) {
                    analysisMsg = "设备在制定检测时间范围的检测结果条数一致的数量少于两个";
                } else if (StringUtils.isEmpty(resultVos)) {
                    analysisMsg = "未检测到存在一址多户的设备";
                } else {
                    analysisMsg = "检测到" + resultVos.size() + "条存在一址多户的告警信息";
                }
            } else {
                analysisMsg = "检测设备数量少于两个";
            }
            return AjaxResult.success(analysisMsg, resultVos);
        } catch (Exception e) {
            log.error("分析一址多户监测结果出现异常，异常原因【{}】,异常详情：", e.getMessage(), e);
            return AjaxResult.error("分析一址多户监测结果失败");
        }

    }

    /**
     * @description：对监测数据进行数据转换
     * @author：Sunup
     * @date：2023/10/24 8:38
     * @param：
     */
    private List<YzdhMonitorDataVo> getTransferMonitorDataList(List<BdzMonitorData> monitorDataList) {
        List<YzdhMonitorDataVo> dataVos = new ArrayList<>();
        if (StringUtils.isNotEmpty(monitorDataList)) {
            Map<String, List<BdzMonitorData>> collect = monitorDataList.stream().collect(Collectors.groupingBy(BdzMonitorData::getDeviceNo));
            dataVos = collect.entrySet().stream().map(c -> new YzdhMonitorDataVo(c.getKey(), c.getValue())).collect(Collectors.toList());
        }
        return dataVos;
    }

    /**
     * @description：计算数组中两个数据之间的差值
     * @author：Sunup
     * @date：2023/10/24 14:07
     * @param：
     */
    private double[] calculateDifference(List<BdzMonitorData> list) {
        double[] difference = new double[list.size() - 1];
        for (int i = 0; i < list.size() - 1; i++) {
            double before = Double.valueOf(list.get(i).getDataValue());
            double after = Double.valueOf(list.get(i + 1).getDataValue());
            double diff = Math.abs(Double.valueOf(after - before));
            difference[i] = diff;
            log.info("Difference between " + before + " and " + after + " is: " + diff);
        }
        return difference;
    }

    /**
     * @description：开始分析一址多户，得出结果
     * @author：Sunup
     * @date：2023/10/25 9:36
     * @param：
     */
    private List<YzdhAnalysisRespVo> analysisYzdhResult(YzdhMonitorDataVo vo1, YzdhMonitorDataVo vo2, String slope, String timeRange) {
        List<YzdhAnalysisRespVo> vos = new ArrayList<>();
        double[] diff1 = calculateDifference(vo1.getListMonitorData());
        double[] diff2 = calculateDifference(vo2.getListMonitorData());
        double[] result = new double[diff1.length];
        for (int i = 0; i < diff1.length; i++) {
//            double chazhi=Math.abs(diff1[i] - diff2[i]);
            double chazhi=diff1[i] - diff2[i];
            BigDecimal bg = new BigDecimal(chazhi);
            double f1 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            result[i] = f1;
        }
        int diffCount = 0;
        int diffStartIndex = 0;
        for (int j = 0; j < result.length; j++) {
            double diff = result[j];
            if (diff < Double.valueOf(slope)) {
                diffCount++;
            } else {
                if (diffCount >= Integer.valueOf(timeRange)) {
                    BdzMonitorData bdzMonitorData1 = vo1.getListMonitorData().get(diffStartIndex);
                    BdzMonitorData bdzMonitorData2 = vo1.getListMonitorData().get(j);
                    recordYzdhAlertInfo(vo1, vo2, slope, vos, bdzMonitorData1, bdzMonitorData2);
                }
                diffStartIndex = j;
                diffCount = 0;
            }
        }
        if (diffCount >= Integer.valueOf(timeRange)) {
            BdzMonitorData bdzMonitorData1 = vo1.getListMonitorData().get(diffStartIndex);
            BdzMonitorData bdzMonitorData2 = vo1.getListMonitorData().get(result.length);
            recordYzdhAlertInfo(vo1, vo2, slope, vos, bdzMonitorData1, bdzMonitorData2);
        }
        log.info("设备：{}和设备：{}之间的差值计算结果：{}", vo1.getDeviceNo(), vo2.getDeviceNo(), result);
        return vos;
    }



    /**
     * @description：记录预警信息
     * @author：Sunup
     * @date：2023/10/25 13:51
     * @param：
     */
    private void recordYzdhAlertInfo(YzdhMonitorDataVo vo1, YzdhMonitorDataVo vo2,
                                     String slope, List<YzdhAnalysisRespVo> vos, BdzMonitorData bdzMonitorData1, BdzMonitorData bdzMonitorData2) {
        String alertTime = DateUtils.dateTimeMinute(bdzMonitorData1.getCreateTime()) + "至" + DateUtils.dateTimeMinute(bdzMonitorData2.getCreateTime());
        String alertDevice = "设备" + vo1.getDeviceNo() + "、" + vo2.getDeviceNo() + "数值变化幅度相似，k值小于" + slope;
        YzdhAnalysisRespVo yzdhAnalysisRespVo = new YzdhAnalysisRespVo();
        yzdhAnalysisRespVo.setAlertTime(alertTime);
        yzdhAnalysisRespVo.setAlertDevice(alertDevice);
        yzdhAnalysisRespVo.setDevice1(vo1.getDeviceNo());
        yzdhAnalysisRespVo.setDevice2(vo2.getDeviceNo());
        log.info("设备警告信息出现，警告内容：{}", JSON.toJSONString(yzdhAnalysisRespVo));
        vos.add(yzdhAnalysisRespVo);
    }

}
