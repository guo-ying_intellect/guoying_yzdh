package com.guoying.yzdh.service;


import com.guoying.common.core.domain.AjaxResult;
import com.guoying.yzdh.domain.BdzMonitorData;
import com.guoying.yzdh.vomodel.YzdhAnalysisReqVo;
import com.guoying.yzdh.vomodel.YzdhMonitorDataQueryReqVo;

import java.util.List;

/**
 * 监测数据Service接口
 *
 * @author guoying
 * @date 2023-10-16
 */
public interface IBdzMonitorDataService
{
    /**
     * 查询监测数据
     *
     * @param dataId 监测数据主键
     * @return 监测数据
     */
    public BdzMonitorData selectBdzMonitorDataByDataId(Long dataId);

    /**
     * 查询监测数据列表
     *
     * @param bdzMonitorData 监测数据
     * @return 监测数据集合
     */
    public List<BdzMonitorData> selectBdzMonitorDataList(BdzMonitorData bdzMonitorData);

    /**
     * 新增监测数据
     *
     * @param bdzMonitorData 监测数据
     * @return 结果
     */
    public int insertBdzMonitorData(BdzMonitorData bdzMonitorData);

    /**
     * 修改监测数据
     *
     * @param bdzMonitorData 监测数据
     * @return 结果
     */
    public int updateBdzMonitorData(BdzMonitorData bdzMonitorData);

    /**
     * 批量删除监测数据
     *
     * @param dataIds 需要删除的监测数据主键集合
     * @return 结果
     */
    public int deleteBdzMonitorDataByDataIds(Long[] dataIds);

    /**
     * 删除监测数据信息
     *
     * @param dataId 监测数据主键
     * @return 结果
     */
    public int deleteBdzMonitorDataByDataId(Long dataId);

    /**
     * 查询监测数据列表并进行转换
     *
     * @param reqVo 请求vo
     * @return 监测数据集合
     */
    public AjaxResult selectYzdhMonitorDataList(YzdhMonitorDataQueryReqVo reqVo);

    /**
     * 物联网返回结果数据处理
     *
     * @param monitordata 监测数据
     * @return 监测数据集合
     */
    public void handleYzdhMonitorData(String monitordata);

    /**
     * @description：分析一址多户监测结果
     * @author：Sunup
     * @date：2023/10/23 17:00
     * @param：
     */
    public AjaxResult analysisYzdhResult(YzdhAnalysisReqVo yzdhAnalysisReqVo);

}
