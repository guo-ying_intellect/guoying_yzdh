package com.guoying.yzdh.service.impl;

import java.util.List;

import com.guoying.common.utils.DateUtils;
import com.guoying.yzdh.domain.BdzYzdhAnylysisParam;
import com.guoying.yzdh.service.IBdzYzdhAnylysisParamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.guoying.yzdh.mapper.BdzYzdhAnylysisParamMapper;

/**
 * 一址多户分析参数Service业务层处理
 *
 * @author guoying
 * @date 2023-10-24
 */
@Service
@Slf4j
public class BdzYzdhAnylysisParamServiceImpl implements IBdzYzdhAnylysisParamService {
    @Autowired
    private BdzYzdhAnylysisParamMapper bdzYzdhAnylysisParamMapper;

    /**
     * 查询一址多户分析参数
     *
     * @param paramId 一址多户分析参数主键
     * @return 一址多户分析参数
     */
    @Override
    public BdzYzdhAnylysisParam selectBdzYzdhAnylysisParamByParamId(Integer paramId) {
        return bdzYzdhAnylysisParamMapper.selectBdzYzdhAnylysisParamByParamId(paramId);
    }

    /**
     * 查询一址多户分析参数列表
     *
     * @param bdzYzdhAnylysisParam 一址多户分析参数
     * @return 一址多户分析参数
     */
    @Override
    public List<BdzYzdhAnylysisParam> selectBdzYzdhAnylysisParamList(BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        return bdzYzdhAnylysisParamMapper.selectBdzYzdhAnylysisParamList(bdzYzdhAnylysisParam);
    }

    /**
     * 新增一址多户分析参数
     *
     * @param bdzYzdhAnylysisParam 一址多户分析参数
     * @return 结果
     */
    @Override
    public int insertBdzYzdhAnylysisParam(BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        try {
            bdzYzdhAnylysisParam.setCreateTime(DateUtils.getNowDate());
            return bdzYzdhAnylysisParamMapper.insertBdzYzdhAnylysisParam(bdzYzdhAnylysisParam);
        } catch (Exception e) {
            log.error("新增一址多户分析参数出现异常，异常原因【{}】,异常详情：", e.getMessage(), e);
            return 0;
        }
    }

    /**
     * 修改一址多户分析参数
     *
     * @param bdzYzdhAnylysisParam 一址多户分析参数
     * @return 结果
     */
    @Override
    public int updateBdzYzdhAnylysisParam(BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        bdzYzdhAnylysisParam.setUpdateTime(DateUtils.getNowDate());
        return bdzYzdhAnylysisParamMapper.updateBdzYzdhAnylysisParam(bdzYzdhAnylysisParam);
    }

    /**
     * 批量删除一址多户分析参数
     *
     * @param paramIds 需要删除的一址多户分析参数主键
     * @return 结果
     */
    @Override
    public int deleteBdzYzdhAnylysisParamByParamIds(Integer[] paramIds) {
        return bdzYzdhAnylysisParamMapper.deleteBdzYzdhAnylysisParamByParamIds(paramIds);
    }

    /**
     * 删除一址多户分析参数信息
     *
     * @param paramId 一址多户分析参数主键
     * @return 结果
     */
    @Override
    public int deleteBdzYzdhAnylysisParamByParamId(Integer paramId) {
        return bdzYzdhAnylysisParamMapper.deleteBdzYzdhAnylysisParamByParamId(paramId);
    }
}
