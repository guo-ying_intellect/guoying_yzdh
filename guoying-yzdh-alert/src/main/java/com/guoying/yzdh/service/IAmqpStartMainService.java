package com.guoying.yzdh.service;

/**
 * @description：订阅接口
 * @author：Sunup
 * @date：2023/10/18 8:20
 */
public interface IAmqpStartMainService {
    /**
     * @description：首次订阅数据
     * @author：Sunup
     * @date：2023/10/18 8:21
     * @param：
     */
    public void amqpStart();



}
