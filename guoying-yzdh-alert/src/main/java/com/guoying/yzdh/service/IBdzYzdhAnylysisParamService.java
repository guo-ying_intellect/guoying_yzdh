package com.guoying.yzdh.service;

import com.guoying.yzdh.domain.BdzYzdhAnylysisParam;

import java.util.List;

/**
 * 一址多户分析参数Service接口
 *
 * @author guoying
 * @date 2023-10-24
 */
public interface IBdzYzdhAnylysisParamService
{
    /**
     * 查询一址多户分析参数
     *
     * @param paramId 一址多户分析参数主键
     * @return 一址多户分析参数
     */
    public BdzYzdhAnylysisParam selectBdzYzdhAnylysisParamByParamId(Integer paramId);

    /**
     * 查询一址多户分析参数列表
     *
     * @param bdzYzdhAnylysisParam 一址多户分析参数
     * @return 一址多户分析参数集合
     */
    public List<BdzYzdhAnylysisParam> selectBdzYzdhAnylysisParamList(BdzYzdhAnylysisParam bdzYzdhAnylysisParam);

    /**
     * 新增一址多户分析参数
     *
     * @param bdzYzdhAnylysisParam 一址多户分析参数
     * @return 结果
     */
    public int insertBdzYzdhAnylysisParam(BdzYzdhAnylysisParam bdzYzdhAnylysisParam);

    /**
     * 修改一址多户分析参数
     *
     * @param bdzYzdhAnylysisParam 一址多户分析参数
     * @return 结果
     */
    public int updateBdzYzdhAnylysisParam(BdzYzdhAnylysisParam bdzYzdhAnylysisParam);

    /**
     * 批量删除一址多户分析参数
     *
     * @param paramIds 需要删除的一址多户分析参数主键集合
     * @return 结果
     */
    public int deleteBdzYzdhAnylysisParamByParamIds(Integer[] paramIds);

    /**
     * 删除一址多户分析参数信息
     *
     * @param paramId 一址多户分析参数主键
     * @return 结果
     */
    public int deleteBdzYzdhAnylysisParamByParamId(Integer paramId);
}
