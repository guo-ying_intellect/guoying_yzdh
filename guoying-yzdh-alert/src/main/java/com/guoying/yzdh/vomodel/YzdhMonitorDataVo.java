package com.guoying.yzdh.vomodel;

import com.guoying.yzdh.domain.BdzMonitorData;
import lombok.Data;

import java.util.List;

/**
 * @description：
 * @author：Sunup
 * @date：2023/10/17 9:35
 */
@Data
public class YzdhMonitorDataVo {
    private String deviceNo;
    List<BdzMonitorData> listMonitorData;

    public YzdhMonitorDataVo(String key, List<BdzMonitorData> value) {
        this.deviceNo = key;
        this.listMonitorData = value;
    }

}
