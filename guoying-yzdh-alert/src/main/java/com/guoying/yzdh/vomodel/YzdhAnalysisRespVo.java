package com.guoying.yzdh.vomodel;

import lombok.Data;

/**
 * @description：分析一址多户监测结果vo
 * @author：Sunup
 * @date：2023/10/17 9:35
 */
@Data
public class YzdhAnalysisRespVo {
    private String alertTime;
    private String alertDevice;
    private String device1;
    private String device2;
}
