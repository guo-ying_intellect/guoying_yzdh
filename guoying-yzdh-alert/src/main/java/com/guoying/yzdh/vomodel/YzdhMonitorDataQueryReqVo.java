package com.guoying.yzdh.vomodel;

import lombok.Data;

/**
 * @description：查询一址多户信息请求参数
 * @author：Sunup
 * @date：2023/10/26 15:43
 */
@Data
public class YzdhMonitorDataQueryReqVo {
    private String queryTime;
}
