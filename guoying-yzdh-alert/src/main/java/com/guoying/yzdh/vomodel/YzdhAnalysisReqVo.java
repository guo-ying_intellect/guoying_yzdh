package com.guoying.yzdh.vomodel;

import lombok.Data;

/**
 * @description：分析一址多户监测结果请求参数
 * @author：Sunup
 * @date：2023/10/17 9:35
 */
@Data
public class YzdhAnalysisReqVo {
    private String dateStart;
    private String dateEnd;
    private String slope;
    private String timeRange;
}
