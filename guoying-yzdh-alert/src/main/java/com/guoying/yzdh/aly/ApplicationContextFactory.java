package com.guoying.yzdh.aly;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

/**
 * @description：上下文工厂类
 * @author：Sunup
 * @date：2023/10/19 8:45
 */
public class ApplicationContextFactory {

    private static ApplicationContext applicationContext;

    public static void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        ApplicationContextFactory.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext(){
       return  ApplicationContextFactory.applicationContext;
    }

    public static Object getBean(Class className){
        return applicationContext.getBean(className);
    }

    public static Object getBean(String beanName){
        return applicationContext.getBean(beanName);
    }

    public static <T> T getBean(String beanName,Class<T> t){
        Object bean = applicationContext.getBean(beanName);

        if(bean != null && t.isAssignableFrom(bean.getClass()) ){
            return (T)bean;
        }

        return null;
    }


}
