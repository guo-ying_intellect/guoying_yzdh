package com.guoying.yzdh.aly;

import com.guoying.yzdh.service.IAmqpStartMainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * @description：服务启动加载类
 * @author：Sunup
 * @date：2023/10/19 9:15
 */
public class AmqpClientStart {

    private final static Logger logger = LoggerFactory.getLogger(AmqpClientStart.class);


    public static void initSystemConfig(ApplicationContext context) {
        try {
            ApplicationContextFactory.setApplicationContext(context);
            startAmqpClientThread();
        } catch (Exception ex) {
            logger.error("系统初始化失败", ex);
            throw new IllegalStateException("系统初始化失败", ex);
        }
    }

    /**
     * @description：执行
     * @author：Sunup
     * @date：2023/10/19 9:15
     * @param：
     */
    private static void startAmqpClientThread() {
        IAmqpStartMainService amqpStartMainService = ApplicationContextFactory.getBean("amqpStartMainServiceImpl",IAmqpStartMainService.class);
        amqpStartMainService.amqpStart();
    }


}
