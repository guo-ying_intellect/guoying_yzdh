package com.guoying.yzdh.domain;
import com.guoying.common.annotation.Excel;
import com.guoying.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 监测数据对象 bdz_monitor_data
 *
 * @author guoying
 * @date 2023-10-16
 */
@Data
public class BdzMonitorData extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 监测数据ID */
    private Long dataId;

    /** 项目ID */
    @Excel(name = "项目ID")
    private Long projectId;

    /** 监测点ID */
    @Excel(name = "监测点ID")
    private Long monitorId;

    /** 设备ID */
    @Excel(name = "设备ID")
    private Long deviceId;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String deviceNo;

    /** 数据类型 */
    @Excel(name = "数据类型")
    private String dataType;

    /** 数据单位 */
    @Excel(name = "数据单位")
    private String dataUnit;

    /** 数据值 */
    @Excel(name = "数据值")
    private String dataValue;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dataId", getDataId())
            .append("projectId", getProjectId())
            .append("monitorId", getMonitorId())
            .append("deviceId", getDeviceId())
            .append("deviceNo", getDeviceNo())
            .append("dataType", getDataType())
            .append("dataUnit", getDataUnit())
            .append("dataValue", getDataValue())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
