package com.guoying.yzdh.domain;

import com.guoying.common.annotation.Excel;
import com.guoying.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 一址多户分析参数对象 bdz_yzdh_anylysis_param
 *
 * @author guoying
 * @date 2023-10-24
 */
public class BdzYzdhAnylysisParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分析参数主键 */
    private Integer paramId;

    /** 时间范围 */
    @Excel(name = "时间范围")
    private String timeRange;

    /** 斜率 */
    @Excel(name = "斜率")
    private String slope;

    /** 是否启用（Y是 N否） */
    @Excel(name = "是否启用", readConverterExp = "Y=是,N=否")
    private String paramStatus;

    public void setParamId(Integer paramId)
    {
        this.paramId = paramId;
    }

    public Integer getParamId()
    {
        return paramId;
    }
    public void setTimeRange(String timeRange)
    {
        this.timeRange = timeRange;
    }

    public String getTimeRange()
    {
        return timeRange;
    }
    public void setSlope(String slope)
    {
        this.slope = slope;
    }

    public String getSlope()
    {
        return slope;
    }
    public void setParamStatus(String paramStatus)
    {
        this.paramStatus = paramStatus;
    }

    public String getParamStatus()
    {
        return paramStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("paramId", getParamId())
            .append("timeRange", getTimeRange())
            .append("slope", getSlope())
            .append("paramStatus", getParamStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
