package com.guoying.yzdh.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
 * @description：阿里云物联网配置
 * @author：Sunup
 * @date：2023/10/17 15:11
 */
@Data
@Component
//使用nacos后，项目通过@ConfigurationProperties装载配置是可以实时刷新的
@ConfigurationProperties(ignoreUnknownFields = false, prefix = "amqp")
public class AliyunIotConfig {
    private String accessKey;
    private String accessSecret;
    private String consumerGroupId;
    //iotInstanceId：实例ID。若是2021年07月30日之前（不含当日）开通的公共实例，请填空字符串。
    private String iotInstanceId;
    //控制台服务端订阅中消费组状态页客户端ID一栏将显示clientId参数。
    //建议使用机器UUID、MAC地址、IP等唯一标识等作为clientId。便于您区分识别不同的客户端。
    private String clientId;
    //${YourHost}为接入域名，请参见AMQP客户端接入说明文档
    private String host;

}
