package com.guoying.yzdh.mapper;



import com.guoying.yzdh.domain.BdzMonitorData;
import com.guoying.yzdh.vomodel.YzdhAnalysisReqVo;
import com.guoying.yzdh.vomodel.YzdhMonitorDataQueryReqVo;

import java.util.List;

/**
 * 监测数据Mapper接口
 *
 * @author guoying
 * @date 2023-10-16
 */
public interface BdzMonitorDataMapper
{
    /**
     * 查询监测数据
     *
     * @param dataId 监测数据主键
     * @return 监测数据
     */
    public BdzMonitorData selectBdzMonitorDataByDataId(Long dataId);

    /**
     * 查询监测数据列表
     *
     * @param bdzMonitorData 监测数据
     * @return 监测数据集合
     */
    public List<BdzMonitorData> selectBdzMonitorDataList(BdzMonitorData bdzMonitorData);

    /**
     * 新增监测数据
     *
     * @param bdzMonitorData 监测数据
     * @return 结果
     */
    public int insertBdzMonitorData(BdzMonitorData bdzMonitorData);

    /**
     * 修改监测数据
     *
     * @param bdzMonitorData 监测数据
     * @return 结果
     */
    public int updateBdzMonitorData(BdzMonitorData bdzMonitorData);

    /**
     * 删除监测数据
     *
     * @param dataId 监测数据主键
     * @return 结果
     */
    public int deleteBdzMonitorDataByDataId(Long dataId);

    /**
     * 批量删除监测数据
     *
     * @param dataIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBdzMonitorDataByDataIds(Long[] dataIds);

    /**
     * @description：查询一址多户监测数据列表
     * @author：Sunup
     * @date：2023/10/23 17:39
     * @param：
     */
    public List<BdzMonitorData> selectYzdhMonitorDataList(YzdhMonitorDataQueryReqVo reqVo);

    /**
     * @description：分析一址多户监测结果
     * @author：Sunup
     * @date：2023/10/23 17:39
     * @param：
     */
    public List<BdzMonitorData> analysisYzdhResult(YzdhAnalysisReqVo yzdhAnalysisReqVo);
}
