package com.guoying.yzdh.controller;

import com.guoying.common.core.domain.AjaxResult;
import com.guoying.yzdh.service.IBdzYzdhAnylysisParamService;
import com.guoying.yzdh.service.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @description：请求websocket
 * @author：Sunup
 * @date：2023/10/25 16:10
 */
@RestController
@RequestMapping("/api/socket")
public class WebSocketController {
    //推送数据接口
    @RequestMapping("/push/{cid}")
    public AjaxResult pushToWeb(@PathVariable String cid, String message) {
        Map<String, Object> result = new HashMap<>();
        try {
            WebSocketServer.sendInfo(message, cid);
            result.put("code", cid);
            result.put("msg", message);
        } catch (IOException e) {
            e.printStackTrace();
            result.put("code", cid);
            result.put("msg", e.getMessage());
        }
        return AjaxResult.success(result);

    }
}
