package com.guoying.yzdh.controller;

import com.guoying.common.annotation.Log;
import com.guoying.common.core.controller.BaseController;
import com.guoying.common.core.domain.AjaxResult;
import com.guoying.common.core.page.TableDataInfo;
import com.guoying.common.enums.BusinessType;
import com.guoying.common.utils.poi.ExcelUtil;
import com.guoying.yzdh.domain.BdzMonitorData;
import com.guoying.yzdh.service.IBdzMonitorDataService;
import com.guoying.yzdh.vomodel.YzdhAnalysisReqVo;
import com.guoying.yzdh.vomodel.YzdhMonitorDataQueryReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 监测数据Controller
 *
 * @author guoying
 * @date 2023-10-16
 */
@RestController
@RequestMapping("/yzdhMonitor")
public class BdzMonitorDataController extends BaseController {
    @Autowired
    private IBdzMonitorDataService bdzMonitorDataService;

    /**
     * 查询监测数据列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BdzMonitorData bdzMonitorData) {
        startPage();
        List<BdzMonitorData> list = bdzMonitorDataService.selectBdzMonitorDataList(bdzMonitorData);
        return getDataTable(list);
    }

    /**
     * 导出监测数据列表
     */
    @Log(title = "监测数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BdzMonitorData bdzMonitorData) {
        List<BdzMonitorData> list = bdzMonitorDataService.selectBdzMonitorDataList(bdzMonitorData);
        ExcelUtil<BdzMonitorData> util = new ExcelUtil<BdzMonitorData>(BdzMonitorData.class);
        util.exportExcel(response, list, "监测数据数据");
    }

    /**
     * 获取监测数据详细信息
     */
    @GetMapping(value = "/{dataId}")
    public AjaxResult getInfo(@PathVariable("dataId") Long dataId) {
        return success(bdzMonitorDataService.selectBdzMonitorDataByDataId(dataId));
    }

    /**
     * 新增监测数据
     */
    @Log(title = "监测数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BdzMonitorData bdzMonitorData) {
        return toAjax(bdzMonitorDataService.insertBdzMonitorData(bdzMonitorData));
    }

    /**
     * 修改监测数据
     */
    @Log(title = "监测数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BdzMonitorData bdzMonitorData) {
        return toAjax(bdzMonitorDataService.updateBdzMonitorData(bdzMonitorData));
    }

    /**
     * 删除监测数据
     */
    @Log(title = "监测数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dataIds}")
    public AjaxResult remove(@PathVariable Long[] dataIds) {
        return toAjax(bdzMonitorDataService.deleteBdzMonitorDataByDataIds(dataIds));
    }

    /**
     * 查询一址多户监测数据列表
     */
    @Log(title = "查询一址多户监测数据列表", businessType = BusinessType.OTHER)
    @PostMapping("/yzdhMonitorDatalist")
    public AjaxResult yzdhMonitorDatalist(@RequestBody YzdhMonitorDataQueryReqVo reqVo) {
        return bdzMonitorDataService.selectYzdhMonitorDataList(reqVo);
    }

    /**
     * 分析一址多户监测结果
     */
    @Log(title = "分析一址多户监测结果", businessType = BusinessType.OTHER)
    @PostMapping("/analysisYzdhResult")
    public AjaxResult analysisYzdhResult(@RequestBody(required = false) YzdhAnalysisReqVo yzdhAnalysisReqVo) {
        System.out.println("xxxxxxxxxxxxxxxxxxxx"+yzdhAnalysisReqVo);
        return bdzMonitorDataService.analysisYzdhResult(yzdhAnalysisReqVo);
    }
}
