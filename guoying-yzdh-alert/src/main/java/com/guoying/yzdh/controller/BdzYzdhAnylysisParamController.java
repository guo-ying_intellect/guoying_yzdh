package com.guoying.yzdh.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.guoying.common.annotation.Log;
import com.guoying.common.core.controller.BaseController;
import com.guoying.common.core.domain.AjaxResult;
import com.guoying.common.core.page.TableDataInfo;
import com.guoying.common.enums.BusinessType;
import com.guoying.common.utils.StringUtils;
import com.guoying.common.utils.poi.ExcelUtil;
import com.guoying.yzdh.domain.BdzYzdhAnylysisParam;
import com.guoying.yzdh.service.IBdzYzdhAnylysisParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 一址多户分析参数Controller
 *
 * @author guoying
 * @date 2023-10-24
 */
@RestController
@RequestMapping("/yzdhParam")
public class BdzYzdhAnylysisParamController extends BaseController {
    @Autowired
    private IBdzYzdhAnylysisParamService bdzYzdhAnylysisParamService;

    /**
     * 查询一址多户分析参数列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        startPage();
        List<BdzYzdhAnylysisParam> list = bdzYzdhAnylysisParamService.selectBdzYzdhAnylysisParamList(bdzYzdhAnylysisParam);
        return getDataTable(list);
    }

    /**
     * 导出一址多户分析参数列表
     */
    @Log(title = "一址多户分析参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        List<BdzYzdhAnylysisParam> list = bdzYzdhAnylysisParamService.selectBdzYzdhAnylysisParamList(bdzYzdhAnylysisParam);
        ExcelUtil<BdzYzdhAnylysisParam> util = new ExcelUtil<BdzYzdhAnylysisParam>(BdzYzdhAnylysisParam.class);
        util.exportExcel(response, list, "一址多户分析参数数据");
    }

    /**
     * 获取一址多户分析参数详细信息
     */
    @GetMapping(value = "/{paramId}")
    public AjaxResult getInfo(@PathVariable("paramId") Integer paramId) {
        return success(bdzYzdhAnylysisParamService.selectBdzYzdhAnylysisParamByParamId(paramId));
    }

    /**
     * 新增一址多户分析参数
     */
    @Log(title = "一址多户分析参数", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        if (StringUtils.isEmpty(bdzYzdhAnylysisParam.getTimeRange()) || StringUtils.isEmpty(bdzYzdhAnylysisParam.getSlope())) {
            return toAjax(false);
        } else {
            return toAjax(bdzYzdhAnylysisParamService.insertBdzYzdhAnylysisParam(bdzYzdhAnylysisParam));
        }
    }

    /**
     * 修改一址多户分析参数
     */
    @Log(title = "一址多户分析参数", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BdzYzdhAnylysisParam bdzYzdhAnylysisParam) {
        if (StringUtils.isEmpty(bdzYzdhAnylysisParam.getTimeRange()) || StringUtils.isEmpty(bdzYzdhAnylysisParam.getSlope())) {
            return AjaxResult.error("持续时长和斜率范围都不能为空");
        } else if (!(bdzYzdhAnylysisParam.getParamId() > 0)) {
            return AjaxResult.error("参数ID不能为空");
        } else {
            return toAjax(bdzYzdhAnylysisParamService.updateBdzYzdhAnylysisParam(bdzYzdhAnylysisParam));
        }
    }

    /**
     * 删除一址多户分析参数
     */
    @Log(title = "一址多户分析参数", businessType = BusinessType.DELETE)
    @DeleteMapping("/{paramIds}")
    public AjaxResult remove(@PathVariable Integer[] paramIds) {
        return toAjax(bdzYzdhAnylysisParamService.deleteBdzYzdhAnylysisParamByParamIds(paramIds));
    }
}
