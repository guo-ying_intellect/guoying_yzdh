package com.guoying.quartz.util;

import org.quartz.JobExecutionContext;
import com.guoying.quartz.domain.SysJob;

/**
 * 定时任务处理（允许并发执行）
 *
 * @author guoying
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
